EXCLUDE_VERSIONS=3.14
EXCLUDE_ARCHS=eldk
include ${EPICS_ENV_PATH}/module.Makefile

EXECUTABLES += bin/mrf
EXECUTABLES += bin/mrf_pva

TEMPLATES=-none-
DBDS=-none-
STARTUPS=-none-

MY_DBS=$(notdir $(wildcard db/*.db) $(wildcard db/*.template))
build: $(addprefix ${BUILD_PATH}/db/,${MY_DBS})
${BUILD_PATH}/db/%: db/%
	${QUIET}${MKDIR} -p ${BUILD_PATH}/db
	${QUIET}echo "Copying db  $<"
	${QUIET}${CP} $< $@


ifneq (${EPICSVERSION},)
#
# Manually copy the files in misc dir
#
MY_MISCS=$(shell find misc/ -type f)

#
# Manually copy the DBD files
#
MY_DBDS=$(notdir $(wildcard dbd/*.dbd))
MY_DBDDIR=${BUILD_PATH}/${EPICSVERSION}/dbd

build: $(addprefix ${MY_DBDDIR}/,${MY_DBDS}) $(addprefix ${BUILD_PATH}/,${MY_MISCS})

${MY_DBDDIR}/%.dbd: dbd/%.dbd
	${QUIET}${MKDIR} -p ${MY_DBDDIR}
	${QUIET}echo "Copying dbd $<"
	${QUIET}${CP} $< $@

${BUILD_PATH}/misc/%: misc/%
	${QUIET}${MKDIR} -p $(dir $@)
	${QUIET}echo "Copying misc $<"
	${QUIET}${CP} $< $@

endif
