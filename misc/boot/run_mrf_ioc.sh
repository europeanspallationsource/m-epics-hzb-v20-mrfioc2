#!/bin/bash

dir=`dirname $0`

. ${dir}/env.sh
. ${dir}/load_modules.sh


if [ "${USE_PVA}" = "yes" ]; then
startPVAServer="startPVAServer"
PVA="_pva"
else
startPVAServer=""
PVA=""
fi

mkdir -p ${IOC_DIR}${PVA}
ln -s -t ${IOC_DIR}${PVA} ${dir}/*
chown -R ioc:ioc ${IOC_DIR}${PVA} /var/log/procServ

export MRF_DIR
export MRF_SYS
export MRF_D
export CHIC_VER
export startPVAServer
export PVA

SUDO="sudo -u ioc -g ioc -E MRF_DIR=${MRF_DIR} MRF_SYS=${MRF_SYS} MRF_D=${MRF_D} CHIC_VER=${CHIC_VER} startPVAServer=${startPVAServer} PVA=${PVA}"


${SUDO} \
/usr/bin/procServ --allow \
                  -L /var/log/procServ/out-${IOCNAME}${PVA} \
                  -i ^C^D \
                  -c ${IOC_DIR}${PVA} \
                  -n ${IOCNAME}${PVA} \
                  -p ${IOC_DIR}${PVA}/${IOCNAME}${PVA}.pid \
                  --holdoff 5 \
                  ${PROCSERV_PORT} \
                  ${MRF_DIR}/3.15.4/bin/centos7-x86_64/mrf${PVA} \
                  mrf_st.cmd

