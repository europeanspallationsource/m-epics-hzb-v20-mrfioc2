#!/bin/bash

dir=`dirname $0`

. ${dir}/env.sh

PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}

CLOCK="$1"
LINK_PV="${MRF_SYS}{${MRF_D}}Link-Sts"
CLK_SP_PV="${MRF_SYS}{${MRF_D}}Link:Clk-SP"
CLK_RB_PV="${MRF_SYS}{${MRF_D}}Link:Clk-I"

#
# This seems necessary, otherwise the backtick versions "freeze"
#  it is also nice to see if the set and read back values are the same
#
caget ${CLK_SP_PV}
caget ${CLK_RB_PV}

#
# Get the current clock as a last resort
#
if [ -z "${CLOCK}" ]; then
  CLOCK=`caget -t ${CLK_SP_PV}`
fi

echo "Fixing clock... ${CLOCK}"

#
# Make sure that we have the clock we want
#
if [ `caget -t ${CLK_RB_PV}` != "${CLOCK}" ]; then
  echo "Clock mismatch"
  caput ${CLK_SP_PV} ${CLOCK}
  sleep 5
else
  sleep 3
fi


while [ `caget -tn ${LINK_PV}` -eq 0 ]; do
  #
  # While the link status is not OK,
  #  set the clock back to the default value
  #
  while [ `caget -tn ${LINK_PV}` -eq 0 ]; do
    caput ${CLK_SP_PV} 124.916
    sleep 5
  done

  #
  # Now, that the link is OK, set the clock
  #  to our value, and check if the link is OK
  #
  caput ${CLK_SP_PV} ${CLOCK}
  sleep 5
done
