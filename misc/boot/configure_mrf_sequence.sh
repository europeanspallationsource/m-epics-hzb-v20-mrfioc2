#!/bin/bash

dir=`dirname $0`

. ${dir}/env.sh

PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}

# Configure the EVR sequencer

# All values in uSec

# event code ${EVT}, 127 is the end of sequence
caput -a ${MRF_SYS}{${MRF_D}-SoftSeq:0}EvtCode-SP   5  14 42     42     42   127 | grep ${MRF_SYS} | cut -d ' ' -f -20

# Defining time at which the event codes are sent
cE42_TS0=$((E42_TS0 + 4))
cE42_TS1=$((E42_TS1 + 4))
cE42_TS2=$((E42_TS2 + 4))
E127=$((E42_TS2 + 10))
caput -a ${MRF_SYS}{${MRF_D}-SoftSeq:0}Timestamp-SP 5   0  ${cE42_TS0}  ${cE42_TS1}  ${cE42_TS2} ${E127} | grep ${MRF_SYS} | cut -d ' ' -f -20

# Commit the sequence to HW
caput ${MRF_SYS}{${MRF_D}-SoftSeq:0}Commit-Cmd 1 >/dev/null
