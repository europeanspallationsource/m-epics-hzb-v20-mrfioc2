dbLoadDatabase("${MRF_DIR}/3.15.4/dbd/mrf${PVA}.dbd")
mrf_registerRecordDeviceDriver(pdbbase)

# Give the EVR in this machine a name, using its PCI address
mrmEvrSetupPCI("EVR1", "1:0.0")

epicsEnvSet(SYS,       "${MRF_SYS}")
epicsEnvSet(D,         "${MRF_D}")

epicsEnvSet(ESS_DRVID, "Chop-Drv-01")
epicsEnvSet(HZB_DRVID, "Chop-Drv-02")

#Set the base clock to 88.0519MHz
epicsEnvSet(FEVT,      "88.0519")
#Set the divider to create a 14Hz event
epicsEnvSet(DIV_E14,   "6289421")

#Set the base clock to 140MHz
epicsEnvSet(FEVT,      "140")
#Set the divider to create a 14Hz event
epicsEnvSet(DIV_E14,   "10000000")

epicsEnvSet(E14_WIDTH, "11904.75")
epicsEnvSet(E42_WIDTH, "11904.75")
epicsEnvSet(E42_DELAY, "0")

epicsEnvSet(E42_TS0, "0")
epicsEnvSet(E42_TS1, "23809523")  # 1,000,000,000 / 42
epicsEnvSet(E42_TS2, "47619047")  # 2,000,000,000 / 42

# Load the EVR database
dbLoadRecords("${MRF_DIR}/db/evr-pcie-300dc.db","EVR=EVR1,SYS=${SYS},D=${D},FEVT=${FEVT}")

# Load the helper database for Robin (Woracek) so he can select the correct phase (align the 14Hz signal with the correct 42Hz signal)
dbLoadRecords("${MRF_DIR}/db/robin.db", "SYS=${SYS},D=${D},E42_TS0=${E42_TS0},E42_TS1=${E42_TS1},E42_TS2=${E42_TS2},DLYGEN_14=DlyGen:0")

# Load the database to setup aliases for Markus (Olsson)
dbLoadRecords("${MRF_DIR}/db/markus.db", "SYS=${SYS},DRIVEID=${ESS_DRVID},DLYGEN=${SYS}{${D}-DlyGen:3}")

# Load chopper timestamping database for HZB chopper
dbLoadRecords("/opt/epics/modules/esschic/${CHIC_VER}/db/esschicTimestampBuffer.template", "PREFIX=${SYS}, DRIVEID=${HZB_DRVID}, TDC_TIME_LINK=${SYS}{${D}}EvtBCnt-I.TIME, BEAMPULSE_TIME_LINK=${SYS}{${D}}EvtECnt-I.TIME, TSARR_N=50")

# Load chopper timestamping database for ESS chopper
dbLoadRecords("/opt/epics/modules/esschic/${CHIC_VER}/db/esschicTimestampBuffer.template", "PREFIX=${SYS}, DRIVEID=${ESS_DRVID}, TDC_TIME_LINK=${SYS}{${D}}EvtACnt-I.TIME, BEAMPULSE_TIME_LINK=${SYS}{${D}}EvtECnt-I.TIME, TSARR_N=50")

# Allow for up to 100us of jitter in the simulated 1 Hz when using "Sys clock"
var(evrMrmTimeNSOverflowThreshold, 100000)

${startPVAServer}
iocInit()

# Get timestamp from system clock
dbpf ${SYS}{${D}}TimeSrc-Sel "Sys. Clock"                   # system clock (2)

#Fix clock
system "/bin/bash fix_mrf_clock.sh ${FEVT}"

#Setup of sequence for 14Hz
dbpf ${SYS}{${D}-SoftSeq:0}RunMode-Sel "Normal"             # normal mode (0), the sequencer re-arms after it has finished
dbpf ${SYS}{${D}-SoftSeq:0}TrigSrc:Scale-Sel "Prescaler 0"  # prescaler 0 (2), set the sequencer to trigger on prescaler 0
dbpf ${SYS}{${D}-SoftSeq:0}TsResolution-Sel "nSec"          # nsec (3), set the sequencer timestamps to nsec
dbpf ${SYS}{${D}-SoftSeq:0}Load-Cmd 1                       # load the sequencer
dbpf ${SYS}{${D}-SoftSeq:0}Enable-Cmd 1                     # enable the sequencer
dbpf ${SYS}{${D}-PS:0}Div-SP ${DIV_E14}                     # ticks of event clock, set the prescaler to trigger on 14Hz by dividing down the event clock

########################
#
# Setup for HZB CHOPPERS
#
########################
# Set up a 14Hz event for the HZB choppers on UnivIO 2
dbpf ${SYS}{${D}-DlyGen:0}Width-SP ${E14_WIDTH}             # time in usec, set the delay generator (pulser 0) width to 1 ms
dbpf ${SYS}{${D}-DlyGen:0}Evt:Trig0-SP 14                   # event number, set the delay DlyGen:00 to trigger on event 14
dbpf ${SYS}{${D}-Out:RB02}Src:Pulse-SP "Pulser 0"           # delay generator 0 (0), connect the output RB02 to DlyGen:0

# Align the rising edge of the 14Hz signal with the falling edge of the 42Hz signal
dbpf ${SYS}:Chop-HZB:EdgeCompS ${E42_WIDTH}

# Set up a 42Hz event for the HZB choppers on UnivIO 3
dbpf ${SYS}{${D}-DlyGen:1}Width-SP ${E42_WIDTH}             # time in usec, set the delay generator (pulser 1) width to 1 ms
dbpf ${SYS}{${D}-DlyGen:1}Delay-SP ${E42_DELAY}
dbpf ${SYS}{${D}-DlyGen:1}Evt:Trig0-SP 42                   # event number, set the delay DlyGen:01 to trigger on event 42
dbpf ${SYS}{${D}-Out:RB03}Src:Pulse-SP "Pulser 1"           # delay generator 1 (1), connect the output RB04 to DlyGen:1

# Set up of UnivIO 1 as Input. Generate Code 11 locally on rising edge.
dbpf ${SYS}{${D}-Out:RB01}Src:Scale-SP "Tri-state"         #61
dbpf ${SYS}{${D}-In:1}Edge-Sel         "Active Rising"
dbpf ${SYS}{${D}-In:1}Trig:Ext-Sel     "Edge"
dbpf ${SYS}{${D}-In:1}Code:Ext-SP      11

# Flanks to esschicTimestampBuffer.template
dbpf ${SYS}{${D}}EvtBCnt-I.FLNK ${SYS}:${HZB_DRVID}:TDC
dbpf ${SYS}{${D}}EvtECnt-I.FLNK ${SYS}:${HZB_DRVID}:Ref

################
#
# Setup for CHIC
#
################
#Set up a 14Hz event for detector trigger/gating on UnivIO 4
dbpf ${SYS}{${D}-DlyGen:2}Evt:Trig0-SP    14
dbpf ${SYS}{${D}-DlyGen:2}Width-SP      2860 #2.86ms
dbpf ${SYS}{${D}-DlyGen:2}Delay-SP         0 #0ms
dbpf ${SYS}{${D}-Out:RB04}Src:Pulse-SP "Pulser 2"           # delay generator 2 (2), connect the output RB04 to DlyGen:2

#Set up a 14Hz event for CHIC on UnivIO 5
dbpf ${SYS}{${D}-DlyGen:3}Evt:Trig0-SP    14
dbpf ${SYS}{${D}-DlyGen:3}Width-SP      2860 #2.86ms
dbpf ${SYS}{${D}-DlyGen:3}Delay-SP         0 #0ms
dbpf ${SYS}{${D}-Out:RB05}Src:Pulse-SP    "Pulser 3"        # delay generator 3 (3), connect the output RB05 to DlyGen:3

# Set up of UnivIO 0 as Input. Generate Code 10 locally on rising edge.
dbpf ${SYS}{${D}-Out:RB00}Src:Scale-SP "Tri-state"         #61
dbpf ${SYS}{${D}-In:0}Edge-Sel         "Active Rising"
dbpf ${SYS}{${D}-In:0}Trig:Ext-Sel     "Edge"
dbpf ${SYS}{${D}-In:0}Code:Ext-SP      10

# Flanks to esschicTimestampBuffer.template
dbpf ${SYS}{${D}}EvtACnt-I.FLNK ${SYS}:${ESS_DRVID}:TDC
dbpf ${SYS}{${D}}EvtECnt-I.FLNK ${SYS}:${ESS_DRVID}:Ref

##########
#
# SEQUENCE
#
##########
#Configure sequencer 0 with event code 14 and 42
system "/bin/bash configure_mrf_sequence.sh"

