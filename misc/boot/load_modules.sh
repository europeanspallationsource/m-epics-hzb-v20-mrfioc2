#!/bin/sh

modprobe uio || exit 1
modprobe parport || exit 1
lsmod | grep -q mrf || insmod /lib/modules/`uname -r`/extra/mrf.ko || exit 1
#modprobe mrf || exit 1
chgrp ioc /dev/uio* || exit 1
chmod 660 /dev/uio* || exit 1
